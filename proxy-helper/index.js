'use strict';
var request = require('request'),
  _ = require('lodash');

function certificateOK(req) {
  var cert = req.socket.getPeerCertificate();
  if (!cert) {
    console.warn(new Date() +
      'Proxy helper: Attempted to connect without certificate.\n' +
      ' ' + req.connection.remoteAddress
    );
    return false;
  }
  if(!_.includes(this.config.allowedClients || [],  cert.subject.CN)) {
    console.warn(new Date() +
      'Proxy helper: Attempted to connect with unallowed certificate name "' + cert.subject.CN +
      '". (' + req.connection.remoteAddress + ')'
    );
    return false;
  }
  return true;
}

function createExpressApp(port) {
  var express = require('express'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    https = require('https'),
    app = express();

  app.use(logger('dev'));
  app.use(bodyParser.json());
  var serverOptions = _.assign(this.config.certificates, {
   requestCert: true,
   rejectUnauthorized: true
  });
  port = port || 1818;
  https.createServer(serverOptions, app).listen(port, function() {
    console.log('Proxy server started at port ' + port);
  });
  return app;
}


var proto = {};
/**
 * Creates an object which emulates a remote object registered
 * with "exposeObject(...)."
 * @param params {{url:String,methods:Array}}
 * @returns {{}}
 */
proto.createProxyObject = function(params) {
  var proxy = {},
    certs = this.config.certificates;
  params.methods.forEach(function(method) {
    proxy[method] = function () {
      //last parameter is assumed to be a callback
      var args = Array.prototype.slice.call(arguments),
        firstArgs = args.slice(0, args.length - 1),
        callback = _.last(args);
      if (!_.isFunction(callback)) {
        throw 'Expected last function to be a callback';
      }
      request.post(_.assign(certs ,{
        url: params.url,
        json: {
          method: method,
          args: firstArgs
        }
      }), function(err, response, body) {
        if (err) {
          return callback(new Error('Error connecting with proxy ' + params.url + '\n' + err.toString()));
        }
        if (response && response.statusCode !== 200) {
          return callback(new Error('Error connecting with proxy. Status code: ' + response.statusCode));
        }
        callback.apply(null, body.args);
      });
    }
  });
  return proxy;
};

/**
 * Makes the objects functions available in the url provided (POST method).
 * @param params {{path:String, object:Object, methods:Array}}
 */
proto.exposeObject = function(params) {
  var exposedMethods = params.methods,
    object = params.object,
    app = this.config.expressApp || createExpressApp.call(this, this.config.port),
    self = this;
  app.post(params.path, function(req, res, next) {
    var args = req.body.args,
      method = req.body.method;
    if (!_.includes(exposedMethods, method)) {
      return next(new Error('The method "' + method + '" is not exposed. ' +
        'Expose it like so: proxyHelper.exposeObject({object: obj, methods:["' + method + '"]})'));
    }
    if (!certificateOK.call(self, req)) {
      return next(new Error('Invalid certificate'));
    }
    object[method].apply(object, args.concat([function() {
      var returned = Array.prototype.slice.call(arguments);
      res.json({args: returned});
    }]));
  });
};

exports.create = function(config) {
  if (!config.certificates) {
    throw 'Proxy helper: certificates not set.';
  }
  if (!config.allowedClients) {
    throw 'Proxy helper: allowedClients not set.';
  }
  var proxyHelper = Object.create(proto);
  proxyHelper.config = config;
  return proxyHelper;
};