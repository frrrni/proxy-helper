
var args = process.argv.slice(2);
console.log(JSON.stringify(args));

var fs = require('fs');
var ph = require('../proxy-helper');

var proxyHelper1 = ph.create({
  allowedClients: ['localhost'],
  certificates: {
    ca: fs.readFileSync('./certs/ca-crt.pem'),
    key: fs.readFileSync('./certs/client1-key.pem'),
    cert: fs.readFileSync('./certs/client1-crt.pem')
  },
  port: 1000
});

var proxyHelper2 = ph.create({
  allowedClients: ['localhost'],
  certificates: {
    ca: fs.readFileSync('./certs/ca-crt.pem'),
    key: fs.readFileSync('./certs/client2-key.pem'),
    cert: fs.readFileSync('./certs/client2-crt.pem')
  },
  port: 2000
});


(function Server1(){
  var exclamator = {
    exclamate: function(word, cb) {
      cb(null, word + '!');
    }
  };

  proxyHelper1.exposeObject({
    object: exclamator,
    path: '/exclamator',
    methods: ['exclamate']
  });
}());

(function Server2(){
  var questioner = {
    question: function(word, cb) {
      cb(null, word + '?');
    }
  };

  proxyHelper2.exposeObject({
    object: questioner,
    path: '/questioner',
    methods: ['question']
  });

}());

(function Server1() {
  var questionerProxy = proxyHelper1.createProxyObject({
    url: 'https://localhost:2000/questioner',
    methods: ['question']
  });
  questionerProxy.question('s�', function(err, question) {
    if(err) {
      throw err;
    }
    console.log(question);
  });
}());


(function Server2(){
  var exclamatorProxy = proxyHelper1.createProxyObject({
    url: 'https://localhost:1000/exclamator',
    methods: ['exclamate']
  });

  exclamatorProxy.exclamate('hola', function(err, exclamated) {
    if(err) {
      throw err;
    }
    console.log(exclamated);
  });
}());


