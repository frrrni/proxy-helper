module.exports = {
  getNumeros: function(inicio, final, cb) {
    var numeros = [];
    if (inicio < 0) {
      return cb(new Error('El inicio no puede ser negativo'));
    }
    for(var i = inicio; i <= final; i++) {
      numeros.push(i);
    }
    cb(null, inicio, numeros);
  },
  getNombres: function(letra, cb) {
    var todos = ['Alberto', 'Beto', 'Cecilia', 'Dar�o', 'Emilio', 'Elias', 'Ferni'];
    var filtrado = todos.filter(function (nombre) {
      return nombre[0] == letra;
    });
    if (filtrado.length == 0) {
      return cb(new Error('asdf'));
    }
    cb(null, filtrado, filtrado.length);
  }
};