var proxyHelper = require('../proxy-helper'),
  fs = require('fs');
proxyHelper.setCertificates({
  key: fs.readFileSync('../../ssl/client1-key.pem'),
  cert: fs.readFileSync('../../ssl/client1-crt.pem'),
  ca: fs.readFileSync('../../ssl/ca-crt.pem')
});
var proxy = proxyHelper.createProxy({
  url: 'https://localhost:1818/proxies/some-object',
  methods: ['getNumeros', 'getNombres']
});

module.exports = proxy;