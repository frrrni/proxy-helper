var assert = require('assert');

module.exports = function start() {
 var someObj = require('./some-object-proxy');
  someObj.getNumeros(3, 5, function(err, inicio, numeros) {
    console.log(err);
    console.log(inicio);
    console.log(numeros);

    assert(err == null);
    assert(inicio == 3);
    assert(numeros[0] == 3);
    assert(numeros[1] == 4);
    assert(numeros[2] == 5);
    console.log('Assertions successful');
  });

  someObj.getNombres('E', function(err, nombres, length) {
    assert(err == null);
    assert(nombres[0] == 'Emilio');
    assert(nombres[1] == 'Elias');
    assert(length == 2);
    console.log('Assertions successful');
  });
};